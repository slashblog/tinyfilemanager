<?php

function get_file_contents_range_based($file)
{
    if (!file_exists($file)) {
        header('HTTP/1.1 404 File does not exist');
        exit;
    }

    $ctype = mime_content_type($file);
    $filesize = filesize($file);

    $offset = 0;
    $length = $filesize;

    if (isset($_SERVER['HTTP_RANGE'])) {
        $partialContent = true;

        if (!preg_match('/^bytes=\d*-\d*$/', $_SERVER['HTTP_RANGE'])) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header('Content-Range: bytes */'.$filesize); // Required in 416.
            exit;
        }

        $range = trim(substr($_SERVER['HTTP_RANGE'], 6));
        $parts = explode('-', $range);
        $start = strlen($parts[0]) > 0 ? $parts[0] : 0;
        $end = strlen($parts[1]) == 0 || $parts[1] >= $filesize ? $filesize : $parts[1];

        if ($start > $end) {
            header('HTTP/1.1 416 Requested Range Not Satisfiable');
            header('Content-Range: bytes */'.$filesize); // Required in 416.
            exit;
        }

        $offset = $start;
        $length = $end - $start;
    } else {
        $partialContent = false;
    }

    if ($partialContent) {
        header('HTTP/1.1 206 Partial Content');
        header('Content-Range: bytes '.$offset.'-'.($offset + $length - 1).'/'.$filesize);
    }

    // output the regular HTTP headers
    header('Content-Type: '.$ctype);
    header('Content-Length: '.$length);
    header('Accept-Ranges: bytes');

    $chunk_size = 1024 * 1024;

    $handle = fopen($file, 'r');
    fseek($handle, $offset);

    if ($handle) {
        $remaining = $length;

        while (!feof($handle) && $remaining > 0) {
            if ($remaining > $chunk_size) {
                echo fread($handle, $chunk_size);
                $remaining -= $chunk_size;
            } else {
                echo fread($handle, $remaining);
                $remaining = 0;
            }
        }

        fclose($handle);
    }
}
