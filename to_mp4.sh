#!/bin/bash

# (C) Copyright 2016-2021

if [ $# -lt 3 ] 
then
    echo "Usage $0 convert|rotate-left|rotate-right|extract <directory> <file> [ <rotate> ]" 1>&2
    exit 1
fi

COMMAND="$1"
DIR="$2"
SRC="$3"
SRC_EXT=${SRC##*.}
DEST="${SRC%.*}"
DEST_EXT='mp4'

if [ -f "$DIR/$DEST" ]
then
    echo "File already exists: $DIR/$DEST" 1>&2
    exit 1
fi

if [ "$COMMAND" = 'rotate-left' -o "$COMMAND" = 'rotate-right' ]
then
    DEST_EXT="${SRC_EXT}"
fi

INPUT="$DIR/$SRC"
OUTPUT="${DIR}/${DEST}"

I=0
OUTPUTX="$OUTPUT"
while [ -f "${OUTPUTX}.${DEST_EXT}" ]
do 
    (( I++ ))
    OUTPUTX="${OUTPUT}-${I}"
done

######################################
# Handle extraction of video - Start #
######################################
if [ "$COMMAND" = 'extract' ]
then
    if [ $# -ne 5 ]
    then
        echo "Parameter not provided" 1>&2
        exit 1
    fi

    OUTPUT="${OUTPUTX}.${DEST_EXT}"
    START="$4"
    END="$5"

    ffmpeg -ss "$START" -i "${INPUT}" -t "$END" -vcodec copy -acodec copy "${OUTPUT}"
    exit 0
fi
####################################
# Handle extraction of video - end #
####################################

####################################
# Handle rotation of video - Start #
####################################
EXTRA=''
if [ "$COMMAND" = 'rotate-left' -o "$COMMAND" = 'rotate-right' ]
then
    OUTPUT="${OUTPUTX}.${DEST_EXT}"

    if [ "$COMMAND" = 'rotate-left' ]
    then
        EXTRA='-vf transpose=2'
    elif [ "$COMMAND" = 'rotate-right' ]
    then
        EXTRA='-vf transpose=1'
    fi

    echo "Executing: ffmpeg -i \"$INPUT\" $EXTRA \"${OUTPUT}\""
 
    ffmpeg -i "$INPUT" $EXTRA "${OUTPUT}" # 2>&1
    exit 0
fi
##################################
# Handle rotation of video - End #
##################################

OUTPUT="${OUTPUTX}.mp4"

VCODEC='copy'
ACODEC='aac'

#ffprobe "$INPUT" 2>&1
CODEC=$(ffprobe "$INPUT" 2>&1 | egrep 'Stream .*: Video' | sed -e 's/.*Video: \([^ ]\+\).*/\1/')
echo "Input Video codec: '$CODEC'"

case "$CODEC" in
    *mpeg4*|*flv1*|*wmv3*|*mpeg1video*|wmv1|wmv2)
        echo "Converting video codec from $CODEC to h264"
        VCODEC=h264
        ;;
esac


ffmpeg -i "$INPUT" $EXTRA -vcodec $VCODEC -acodec $ACODEC "${OUTPUT}" # 2>&1
