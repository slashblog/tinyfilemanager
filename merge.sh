#!/bin/bash

# (C) Copyright 2016-2021 

#set -x

if [ $# -le 2 ]
then
    echo "Usage <output-file> <input-file-1> <input-file-2> [ <input-file-3> ... ]" 1>&2
    exit 1
fi

OUTPUT=$1

if [ -f "$OUTPUT" ]
then
    echo "File already exists: $OUTPUT" 1>&2
    exit 2
fi

shift
INPUT_FILE="/tmp/$$.txt"

> "$INPUT_FILE"

while [ $# -gt 1 ]
do
    ESCAPED_PATH=$(echo "$1" | sed -e 's/ /\\ /g')
    echo "file ${ESCAPED_PATH}" >> "${INPUT_FILE}"
    shift
done

ffmpeg -f concat -safe 0 -i "${INPUT_FILE}" -c copy "${OUTPUT}"
rm -f "${INPUT_FILE}"
