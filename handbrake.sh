#!/bin/bash

# (C) Copyright 2016-2021

if [ $# -ne 3 ] 
then
    echo "Usage $0 <directory> <file> <preset>" 1>&2
    exit 1
fi

DIR="$1"
SRC="$2"
PRESET="$3"
DEST="${SRC%.*}"

if [ -f "$DIR/$DEST" ]
then
    echo "File already exists: $DIR/$DEST" 1>&2
    exit 1
fi

HandBrakeCLI -i "$DIR/$SRC" -o "${DIR}/${DEST}-hb.mp4" --preset="${PRESET}"
